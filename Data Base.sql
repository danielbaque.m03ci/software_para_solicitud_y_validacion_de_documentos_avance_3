/*==============================================================*/
/*=================      DBMS name: My SQL     =================*/
/*==============================================================*/


/*==============================================================*/
/* Table: CONTACTO                                              */
/*==============================================================*/
create table CONTACTO
(
   ID_USER              int not null  comment '',
   NOMBRE               varchar(50) not null  comment '',
   EMAIL_USER           varchar(60) not null  comment '',
   ASUNTO_CONT          varchar(50)  comment '',
   MENSAJE_CONT         text  comment '',
   primary key (ID_USER, NOMBRE, EMAIL_USER)
);

/*==============================================================*/
/* Table: DETALLE_PROCESO                                       */
/*==============================================================*/
create table DETALLE_PROCESO
(
   ID_USER              int  comment '',
   NOMBRE               varchar(50)  comment '',
   EMAIL_USER           varchar(60)  comment '',
   ID_PROGRAMA          int  comment '',
   ESTADO_PROGRAMA      varchar(20)  comment ''
);

/*==============================================================*/
/* Table: PROGRAMA                                              */
/*==============================================================*/
create table PROGRAMA
(
   ID_PROGRAMA          int not null auto_increment  comment '',
   ESTADO_PROGRAMA      varchar(20) not null  comment '',
   primary key (ID_PROGRAMA, ESTADO_PROGRAMA)
);

/*==============================================================*/
/* Table: PROMOCION                                             */
/*==============================================================*/
create table PROMOCION
(
   NOMBRE_PROGRAMA      varchar(40) not null  comment '',
   ID_PROGRAMA          int  comment '',
   ESTADO_PROGRAMA      varchar(20)  comment '',
   DESCRIPCION_PRGRAMA  text  comment '',
   MATERIAL_PROGRAMA    tinyint  comment '',
   primary key (NOMBRE_PROGRAMA)
);

/*==============================================================*/
/* Table: RECATEGORIZACION                                      */
/*==============================================================*/
create table RECATEGORIZACION
(
   NOMBRE_PROGRAM       varchar(40) not null  comment '',
   ID_PROGRAMA          int  comment '',
   ESTADO_PROGRAMA      varchar(20)  comment '',
   DESCRIPCION_PROGRAM  text  comment '',
   MATERIAL_PROGRAM     tinyint  comment '',
   primary key (NOMBRE_PROGRAM)
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO
(
   ID_USER              int(30) not null auto_increment  comment '',
   NOMBRE               varchar(50) not null  comment '',
   USER                 varchar(20) not null  comment '',
   TELEFONO_USER        varchar(15) not null  comment '',
   EMAIL_USER           varchar(60) not null  comment '',
   CONTRASENA_USER      longtext not null  comment '',
   primary key (ID_USER, NOMBRE, EMAIL_USER)
);

alter table CONTACTO add constraint FK_CONTACTO_RELATIONS_USUARIO foreign key (ID_USER, NOMBRE, EMAIL_USER)
      references USUARIO (ID_USER, NOMBRE, EMAIL_USER) on delete restrict on update restrict;

alter table DETALLE_PROCESO add constraint FK_DETALLE__RELATIONS_USUARIO foreign key (ID_USER, NOMBRE, EMAIL_USER)
      references USUARIO (ID_USER, NOMBRE, EMAIL_USER) on delete restrict on update restrict;

alter table DETALLE_PROCESO add constraint FK_DETALLE__RELATIONS_PROGRAMA foreign key (ID_PROGRAMA, ESTADO_PROGRAMA)
      references PROGRAMA (ID_PROGRAMA, ESTADO_PROGRAMA) on delete restrict on update restrict;

alter table PROMOCION add constraint FK_PROMOCIO_RELATIONS_PROGRAMA foreign key (ID_PROGRAMA, ESTADO_PROGRAMA)
      references PROGRAMA (ID_PROGRAMA, ESTADO_PROGRAMA) on delete restrict on update restrict;

alter table RECATEGORIZACION add constraint FK_RECATEGO_RELATIONS_PROGRAMA foreign key (ID_PROGRAMA, ESTADO_PROGRAMA)
      references PROGRAMA (ID_PROGRAMA, ESTADO_PROGRAMA) on delete restrict on update restrict;

