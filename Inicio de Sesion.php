<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>SSVDR - FACCI</title>
    <meta name="viewport" content="width=device-width, user-scalable=yes, 
        initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="css/EstilosIS.css">
    <link rel="icon" type="image/png" href="images/LOGO-ULEAM-VERTICAL.png">

</head>

<body>
    <header class="header">
        <div class="container0">
            <figure class="logo">
                <br>
                <h1>
                    <strong> Proceso de Solicitud y Validación de Documentos para Promociones y Recategorizaciones  </strong>  
               </h1>
            </figure>

        </div>

        <div class="container">
            <figure class="logo">
                <img src="images/LOGO-ULEAM-VERTICAL.png" alt="logo" />
            </figure>
            <nav class="menu">

                <ol>
				    <li>
                        <a class="link" href="index.html"> <b>Inicio</b> </a>
                    </li>
                    <li>
                        <a class="link" href="Registro.php"> <b>Regístrate</b> </a>

                    </li>
					<li>  
                        <a class="link"  href="Nosotros.html" > <b>Nosotros</b> </a>  
                   </li>
					<li>  
                        <a class="link" href="contactos.html"> <b>Contáctanos</b> </a>  
                   </li>
                    
                </ol>
            </nav>
        </div>
    </header>
</body>

    <body>
        <!-- Proceso de Conexion a la Base de Datos -->
	    <?php
		if (isset ($_POST ['ENVIAR']))
		{
			$CORR=$_POST ["correos"];	
			$PAS=$_POST ["contra"];
			$link=mysqli_connect("localhost","root");
			if(mysqli_connect_errno())
				{ 
				printf("Error de Conexion %5\n", mysqli_connect_errno);
				exit();
				}
			    mysqli_select_db($link,"usuarios");
			    $Consulta="Select*from informacion where EMAIL='$CORR' AND CONTRASENA='$PAS'";
			    $R=mysqli_query($link,$Consulta);
			    if ($R)
					if (mysqli_error($link))
						{
						echo "error";
						exit();
						}
					$row=mysqli_fetch_array($R);
					if(!empty($row))
						{
                        $emmail=$row["EMAIL"];
						$passss=$row["CONTRASENA"];
						}
					if($CORR == $emmail && $PAS == $passss){
					header("location: Home.html");
					}
						
else
					{
    echo'<script type="text/javascript">
    alert("Email o Contraseña Incorrecta");
    window.location.href="Inicio de Sesion.php";
    </script>';				
					}
				mysqli_close($link);   
			}
		else
			{
	?>
        <form action="Inicio de Sesion.php" class="formulario"  id="form" method="POST">
            <h2>Inicio de Sesión</h2>
            <br>

            <div class="contenedor">
    

                <div class="input-contenedor">
                    <i class="fas fa-envelope icon"></i>
                    <input type="email" placeholder="Correo Electronico" id="correoo" name="correos" required>
                </div>

                <div class="input-contenedor">
                    <i class="fas fa-key icon"></i>
                    <input type="password" placeholder="Contraseña" id="contrase" name="contra" required>
                </div>

                <div class="input-contenedor-button">
                   <button type="submit" class="button" name="ENVIAR"> Iniciar Sesión </button>
                    <br>
                    <br>
                    <p>¿No tienes una cuenta? <a class="link" href="Registro.php">Regístrate </a></p>
					<br>
					<p>¿Olvidaste tu contraseña? <a class="link" href="Recuperar.html">Olvide mi contraseña </a></p>
                </div>

           
 <?php
		}
	?>
		 </div>
		  <script src="validarsesion.js"></script>
    </body>

</html>