
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>SSVDR - FACCI</title>
  <link rel="stylesheet" href="css/EstRegistro.css">
  <link rel="icon" type="image/png" href="images/LOGO-ULEAM-VERTICAL.png">
 
</head>


<body>
    <header class="header">
        <div class="container0">
            <figure class="logo">
                <br>
                <h1>
                  <strong> Proceso de Solicitud y Validación de Documentos para Promociones y Recategorizaciones  </strong>  
             </h1>
            </figure>
           
          
        </div>
        
        <div class="container">
            <figure class="logo">
                <img src="images/LOGO-ULEAM-VERTICAL.png"  alt="logo"/>
            </figure>
            <nav class="menu">
                
                <ol>
	                <li>  
                         <a class="link" href="index.html"> <b>Inicio</b>  </a>  
                    </li>
                    <li>  
                         <a class="link" href="Inicio de Sesion.php" > <b>Iniciar sesión</b>  </a>  
                         
                    </li>
					<li>  
                        <a class="link" href="nosotros.html" > <b>Nosotros</b> </a>  
                   </li>
                    <li>  
                         <a class="link" href="contactos.html" > <b>Contáctanos</b> </a>  
                    </li>
                </ol>
            </nav>
        </div>
    </header>

    <!-- Proceso para registrar los datos ingresados a la Base de Datos -->
    <?php
		if (isset ($_POST ['ENVIAR']))
		{
			$NOM=$_POST ["nombre"];	
			$APE=$_POST ["apellidos"];
			$CELL=$_POST ["cell"];
			$CONTRA=$_POST ["pass"];
			$EMAIL=$_POST ["emaill"];
			$link =mysqli_connect("localhost","root");
			if (mysqli_connect_errno())
			{
				printf("Error De Conexion %5\n",mysqli_connect_errno);
				exit ();
			}
			mysqli_select_db ($link ,"usuarios");
			$r=mysqli_query($link, "Insert into informacion(NOMBRE,APELLIDO,TELEFONO,CONTRASENA,EMAIL)values('$NOM','$APE','$CELL','$CONTRA','$EMAIL')");
			if (!$r)
				printf ("Error %5\n",mysqli_error($link));
			else
				 echo'<script type="text/javascript">
    alert("Datos Ingresados");
    window.location.href="Registro.php";
    </script>';	
			mysqli_close($link);
		}
		else
		{
	?>
  <div class="container">
    <form action="Registro.php" class="form" name="form" id="form" autocomplete="off" method="POST">
      <p class="form-titulo">Regístrate</p>
        <div class="form-input myname">
          <input type="text" name="nombre" id="myname" onkeypress="return letrass(event);" maxlength="20" required>
          <label for="myname">Nombre</label>
        </div>
        <div class="form-input surname">
          <input type="text" name="apellidos" id="surname" onkeypress="return letrass(event);" maxlength="20" required>
          <label for="surname">Apellidos</label>
        </div>
		 <div class="form-input cell">
        <input type="text" name="cell" id="cell" onkeypress="return numeros(event);" maxlength="10" required>
        <label for="mobile">Celular</label>
      </div>
	  <div class="form-input password">
        <input type="password" name="pass" id="password" maxlength="14"required>
        <label for="password">Contraseña</label>
      </div>
        <div class="form-input email">
          <input type="email" name="emaill" id="email"  required>
          <label for="email">Correo electrónico</label>
        </div>
     
      
      <div class="form-boton" >
        <input type="submit" name="ENVIAR" value="Enviar" onclick="validarcamvacion();">
      </div>

	  	<?php
		}
	?>
  </div>
  <script src="form.js"></script>

</body>
</html>