const tabla = document.querySelector('#lista-Datos tbody');

// Funcion para mostrar los datos del archivo "Datos.json"
function cargarDatos() {
    fetch('ContDesarrolladores.json')
        .then(respuesta => respuesta.json()) 
        .then(Datos => {
            Datos.forEach(Dato => {
                const row = document.createElement('tr');
                row.innerHTML += `
                    <td>${Dato.NOMBRES}</td>
                    <td>${Dato.TELEFONO}</td>
                    <td>${Dato.EMAIL}</td>
                `;
                tabla.appendChild(row);
            });
        }) 
        .catch(error => console.log('Se ha presentado un error: ' + error.message))
}
cargarDatos();
